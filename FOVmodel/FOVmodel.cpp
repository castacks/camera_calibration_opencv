#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>

using namespace std;
using namespace cv;

#define PATTERNSIZE_X 11
#define PATTERNSIZE_Y 7

bool read_images(char *dir_path, vector<vector <Point2f> > &imagePoints, vector<vector <Point3f> > &objectPoints, vector<string> &image_name, Size &image_size)
{
	int					i, n=0;
	DIR					*d;
	struct dirent		*dir;
	Mat					img, img_gray;
	char				str[256], path[256];
	const Size			patternSize(PATTERNSIZE_X, PATTERNSIZE_Y);
	TermCriteria		criteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 40, 0.001);
	vector<Point2f>		image_points;
	vector<Point3f>		object_points;


	d = opendir(dir_path);
	if(d){
		while((dir = readdir(d)) != NULL){
			sprintf(str, dir->d_name);
			if(str[strlen(str)-3]!='j' || str[strlen(str)-2]!='p' || str[strlen(str)-1]!='g')
				continue;
			printf("%s   ", str);
			sprintf(path, "%s%s", dir_path, str);
			img = imread(path,1);
			if(img.data==NULL){
				printf("is not image file. Skip\n");
				continue;
			}
			cvtColor(img, img_gray, CV_BGR2GRAY);
			if(!findChessboardCorners(img_gray, patternSize, image_points)){
				printf("---enough corners not found. Skip\n");
				continue;
			}

			cornerSubPix(img_gray, image_points, Size(11,11), Size(-1,-1), criteria);
			for(i=0;i<patternSize.area();i++)
				object_points.push_back(Point3f(static_cast<float>(i%patternSize.width*19.4), static_cast<float>(i/patternSize.width*19.4), 0.0));

			imagePoints.push_back(image_points);
			objectPoints.push_back(object_points);

			image_name.push_back(path);

			image_points.clear();
			object_points.clear();


			for(i=0;i<(int)imagePoints[n].size();i+=1)
				circle(img, imagePoints[n][i], 3, Scalar(0,0,255), -1, 4);
			imshow("img", img);
			printf("...All corners found from the image.\n");
			waitKey(0);
			n++;
	    }
		closedir(d);
		destroyWindow("img");
		waitKey(10);
	}
	image_size = img.size();
	return true;
}

int main(int argc, char **argv)
{

	char						dir_path[512]="../calib_images_120deg_camera/";
	char						str[512]="";

	vector<string>				image_name;

	vector<vector<Point2f> >	imagePoints;
	vector<vector<Point3f> >	objectPoints;

	int							i,j;

	//Camera parameters
	Mat							cameraMatrix, newCameraMatrix;
	Mat							distCoeffs(0,8,CV_64F);
	vector<Mat>					rotationVectors;
	vector<Mat>					translationVectors;
	int							flag = CV_CALIB_FIX_K3; //= CV_CALIB_RATIONAL_MODEL; //CV_CALIB_FIX_K3;
	Size						imageSize, newImageSize;


	double						apertureWidth = 0.0, apertureHeight = 0.0 ;			//double apertureWidth = 23.5, apertureHeight = 15.6 ;  // �Z���T�̕����I�ȕ��E����
	double						fovx = 0, fovy = 0;                                 //
	double						focalLength = 0;                                    //
	Point2d						principalPoint(0.0,0.0);                            //
	double						aspectRatio = 0;                                    //

	FILE						*fp;

	Mat							img, undist_img, map1, map2;
	double						rms;

//	sprintf(dir_path, argv[1]);
	int str_len = strlen(dir_path);
	if(dir_path[str_len-1]!='/'){
		dir_path[str_len]='/';
		dir_path[str_len+1]=0;
	}

	read_images(dir_path, imagePoints, objectPoints, image_name, imageSize);


	img = imread(image_name[0]);
	imshow("raw_img", img);

	Point2d pd,pu,ppu, center_u, pmin, pmax, center_d;
	double rd, ru, rrd,rru,rm;
	double s=1.0;
	int u,v, uu,vv;


	center_d.x=297.0; //(double)img.cols/2.0;
	center_d.y=232.0; //(double)img.rows/2.0;

	rm = 2*sqrt(center_d.x*center_d.x+center_d.y*center_d.y);


	uchar				*undist_img_ptr;


	while(1){
		pmin = Point2d(0,0)-center_d;
		rd = sqrt(pmin.x*pmin.x+pmin.y*pmin.y);
		rrd = rd/rm;
		rru = tan(rrd*s);
		ru = rru*rm;
		pmin = pmin*(ru/rd);

		pmax = Point2d(img.cols,img.rows)-center_d;
		rd = sqrt(pmax.x*pmax.x+pmax.y*pmax.y);
		rrd = rd/rm;
		rru = tan(rrd*s);
		ru = rru*rm;
		pmax = pmax*(ru/rd);
		center_u = Point2d(0,0)-pmin;
		uu = (int)((pmax - pmin).x);
		vv = (int)((pmax - pmin).y);
		undist_img = Mat::zeros(vv,uu, img.type());



		for(vv=0; vv<undist_img.rows; vv++){
			undist_img_ptr = undist_img.ptr(vv);
			for(uu=0; uu<undist_img.cols; uu++){
				pu.x = uu - center_u.x;
				pu.y = vv - center_u.y;
				ru = sqrt(pu.x*pu.x + pu.y*pu.y);
				rru = ru/rm;
				rrd = 1/s*atan(rru);
				rd = rrd*rm;
				u = pu.x * (rd/ru) + center_d.x;
				v = pu.y * (rd/ru) + center_d.y;

				if(u<0 || img.cols<u || v<0 || img.rows<v) continue;
				undist_img_ptr[uu*3+0] = img.ptr(v)[u*3+0];
				undist_img_ptr[uu*3+1] = img.ptr(v)[u*3+1];
				undist_img_ptr[uu*3+2] = img.ptr(v)[u*3+2];
			}
		}

		imshow("undist", undist_img);

		int keycode = waitKey(0);
		undist_img.release();

		if(keycode==27) break; //ESC
		else if(keycode==65361); //Left
		else if(keycode==65362){
			s+=0.01; //Up
		}else if(keycode==65363); //Right
		else if(keycode==65364){
			s-=0.01;//Down
		}
		printf("s:%lf\n",s);
	}


	destroyWindow("undist");
	destroyWindow("raw_img");




	return 1;
}
