/*
#ifdef _DEBUG
    //Debug���[�h�̏ꍇ
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_core245d.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_imgproc245d.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_highgui245d.lib")
	#pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_calib3d245d.lib")
#else
    //Release���[�h�̏ꍇ
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_core245.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_imgproc245.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_highgui245.lib")
	#pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_calib3d245.lib")
#endif
*/



#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>

using namespace std;
using namespace cv;

#define PATTERNSIZE_X 11
#define PATTERNSIZE_Y 7

double computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
                          const vector<vector<Point2f> >& imagePoints,
                          const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                          const Mat& cameraMatrix , const Mat& distCoeffs,
                          vector<float>& perViewErrors)
{
vector<Point2f> imagePoints2;
int i, totalPoints = 0;
double totalErr = 0, err;
perViewErrors.resize(objectPoints.size());

for( i = 0; i < (int)objectPoints.size(); ++i )
{
  projectPoints( Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,  // project
                                       distCoeffs, imagePoints2);
  err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);              // difference

  int n = (int)objectPoints[i].size();
  perViewErrors[i] = (float) std::sqrt(err*err/n);                        // save for this view
  totalErr        += err*err;                                             // sum it up
  totalPoints     += n;
}

return std::sqrt(totalErr/totalPoints);              // calculate the arithmetical mean
}



bool read_images(char *dir_path, vector<vector <Point2f> > &imagePoints, vector<vector <Point3f> > &objectPoints, vector<string> &image_name, Size &image_size)
{
	int					i, n=0;
	DIR					*d;
	struct dirent		*dir;
	Mat					img, img_gray;
	char				str[256], path[256];
	const Size			patternSize(PATTERNSIZE_X, PATTERNSIZE_Y);
	TermCriteria		criteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 40, 0.001);
	vector<Point2f>		image_points;
	vector<Point3f>		object_points;


	d = opendir(dir_path);
	if(d){
		while((dir = readdir(d)) != NULL){
			sprintf(str, dir->d_name);
			if(str[strlen(str)-3]!='j' || str[strlen(str)-2]!='p' || str[strlen(str)-1]!='g')
				continue;
			printf("%s   ", str);
			sprintf(path, "%s%s", dir_path, str);
			img = imread(path,1);
			if(img.data==NULL){
				printf("is not image file. Skip\n");
				continue;
			}
			cvtColor(img, img_gray, CV_BGR2GRAY);
			if(!findChessboardCorners(img_gray, patternSize, image_points)){
				printf("---enough corners not found. Skip\n");
				continue;
			}

			cornerSubPix(img_gray, image_points, Size(11,11), Size(-1,-1), criteria);
			for(i=0;i<patternSize.area();i++)
				object_points.push_back(Point3f(static_cast<float>(i%patternSize.width*0.0194), static_cast<float>(i/patternSize.width*0.0194), 0.0));

			imagePoints.push_back(image_points);
			objectPoints.push_back(object_points);

			image_name.push_back(path);

			image_points.clear();
			object_points.clear();


			for(i=0;i<(int)imagePoints[n].size();i+=1)
				circle(img, imagePoints[n][i], 3, Scalar(0,0,255), -1, 4);
			imshow("img", img);
			printf("...All corners found from the image.\n");
			waitKey(10);
			n++;
	    }
		closedir(d);
		destroyWindow("img");
		waitKey(10);
	}
	image_size = img.size();
	return true;
}




int main(int argc, char **argv)
{

	char						dir_path[512]="./calib_images_120deg_camera/";
	char						str[512]="";

	vector<string>				image_name;

	vector<vector<Point2f> >	imagePoints;
	vector<vector<Point3f> >	objectPoints;

	int							i,j;

	//Camera parameters
	Mat							cameraMatrix, newCameraMatrix;
	Mat							distCoeffs(1,8,CV_64F);
	vector<Mat>					rotationVectors;
	vector<Mat>					translationVectors;
	int							flag = CV_CALIB_FIX_K3; //= CV_CALIB_RATIONAL_MODEL; //CV_CALIB_FIX_K3;
	Size						imageSize, newImageSize;


	double						apertureWidth = 0.0, apertureHeight = 0.0 ;			//double apertureWidth = 23.5, apertureHeight = 15.6 ;  // �Z���T�̕����I�ȕ��E����
	double						fovx = 0, fovy = 0;                                 //
	double						focalLength = 0;                                    //
	Point2d						principalPoint(0.0,0.0);                            //
	double						aspectRatio = 0;                                    //

	FILE						*fp;

	Mat							img, undist_img, map1, map2;
	double						rms;

	sprintf(dir_path, argv[1]);
	int str_len = strlen(dir_path);
	if(dir_path[str_len-1]!='/'){
		dir_path[str_len]='/';
		dir_path[str_len+1]=0;
	}

	read_images(dir_path, imagePoints, objectPoints, image_name, imageSize);
	
	flag = 0;
	//flag = CV_CALIB_FIX_K3 | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6;
	//flag = CV_CALIB_ZERO_TANGENT_DIST;
	//flag = CV_CALIB_ZERO_TANGENT_DIST | CV_CALIB_FIX_K3 | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6;
	//flag = CV_CALIB_FIX_PRINCIPAL_POINT;
	//flag = CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6;
	memset(str, 0, 512);
	sprintf(str, "%scam_info.yaml", dir_path);
	fp=fopen(str, "w");

	rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rotationVectors, translationVectors, flag);
	printf("rms = %lf\n", rms);
	calibrationMatrixValues(cameraMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectRatio);

	newCameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1.0,  newImageSize, 0, false);
	initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat::eye(3,3,CV_32F), newCameraMatrix, imageSize, CV_32FC1, map1, map2);

	for(i=0;i<image_name.size();i++){
		img = imread(image_name[i]);
		//undistort(img, undist_img, cameraMatrix, distCoeffs);
		remap(img, undist_img, map1,map2,INTER_AREA, BORDER_CONSTANT, 0);
		imshow("undist", undist_img);
		imshow("raw_img", img);
		waitKey(0);
	}
	destroyWindow("undist");
	destroyWindow("raw_img");



	fprintf(fp, "image_width: %d\n", imageSize.width);
	fprintf(fp, "image_height: %d\n", imageSize.height);
	fprintf(fp, "camera_name: head_camera\n");
	fprintf(fp, "camera_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, 0, %.13lf, %.13lf, 0, 0, 1]\n", cameraMatrix.at<double>(0,0), cameraMatrix.at<double>(0,2), cameraMatrix.at<double>(1,1), cameraMatrix.at<double>(1,2));
	fprintf(fp, "distortion_model: plumb_bob\n");
	fprintf(fp, "distortion_coefficients:\n");
	fprintf(fp, "  rows: 1\n");
	fprintf(fp, "  cols: 5\n");
	fprintf(fp, "  data: [%.17lf, %.17lf, %.17lf, %.17lf, %.17lf]\n", distCoeffs.at<double>(0,0), distCoeffs.at<double>(0,1), distCoeffs.at<double>(0,2), distCoeffs.at<double>(0,3), distCoeffs.at<double>(0,4));
	fprintf(fp, "rectification_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [1, 0, 0, 0, 1, 0, 0, 0, 1]\n");
	fprintf(fp, "projection_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 4\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, 0, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n", cameraMatrix.at<double>(0,0), cameraMatrix.at<double>(0,2), cameraMatrix.at<double>(1,1), cameraMatrix.at<double>(1,2));
	fprintf(fp, "  #data: [%.13lf, 0, %.13lf, 0, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n", newCameraMatrix.at<double>(0,0), newCameraMatrix.at<double>(0,2), newCameraMatrix.at<double>(1,1), newCameraMatrix.at<double>(1,2));

	fprintf(fp, "#\n");
	fprintf(fp, "#Output field of view in degrees along the horizontal sensor axis: %lf\n", fovx);
	fprintf(fp, "#Output field of view in degrees along the vertical sensor axis: %lf\n", fovy);
	fprintf(fp, "#Focal length of the lens in mm: %lf\n", focalLength);
	fprintf(fp, "#Principal point (x,y) in mm: %lf, %lf\n", principalPoint.x, principalPoint.y);
	fprintf(fp, "#Aspect ratio fx/fy: %lf\n", aspectRatio);
	fprintf(fp, "#RMS error of reprojection: %lf\n", rms);



	fclose(fp);

	return 1;
}


/*
 int main(int argc, char **argv)
{

	char						dir_path[512]="./calib_images_120deg_camera/";
	char						str[512]="";

	vector<vector<Point2f> >		imagePoints;
	vector<vector<Point3f> >		objectPoints;

	int							i,j;

	//Camera parameters
	Mat							cameraMatrix;
	Mat							distCoeffs(0,8,CV_64F);
	vector<Mat>					rotationVectors;
	vector<Mat>					translationVectors;
	int							flag = CV_CALIB_FIX_K3; //= CV_CALIB_RATIONAL_MODEL; //CV_CALIB_FIX_K3;
	Size						imageSize;


	double						apertureWidth = 0.0, apertureHeight = 0.0 ;			//double apertureWidth = 23.5, apertureHeight = 15.6 ;  // �Z���T�̕����I�ȕ��E����
	double						fovx = 0, fovy = 0;                                 // �o�͂���鐅���i�����j���ɂ�������p�i�x�P�ʁj
	double						focalLength = 0;                                    // mm�P�ʂŕ\���ꂽ�����Y�̏œ_����
	Point2d						principalPoint(0.0,0.0);                             // �s�N�Z���P�ʂŕ\���ꂽ�����Y�̏œ_����
	double						aspectRatio = 0;                                    // �A�X�y�N�g��


	FILE						*fp;
	int							n_ver=1, i_ver;

	sprintf(dir_path, argv[1]);
	int str_len = strlen(dir_path);
	if(dir_path[str_len-1]!='/'){
		dir_path[str_len]='/';
		dir_path[str_len+1]=0;
	}

	imageSize = read_images(dir_path, imagePoints, objectPoints);




	for(i_ver=0;i_ver<n_ver;i_ver++){
	if(i_ver==0)		flag = CV_CALIB_FIX_K3;
	else if(i_ver==1)	flag = 0;
	else				flag = CV_CALIB_RATIONAL_MODEL;
	memset(str, 0, 512);
	sprintf(str, "%scam_param_%d.csv", dir_path, i_ver);
	fp=fopen(str, "w");
	fprintf(fp, "#,fovx,fovy, focalLength, principalPoint.x, principalPoint.y, aspectRatio, k1,k2,p1,p2,k3,k4,k5,k6,,");
	for(i=0;i<3;i++) fprintf(fp, "A[%d][0],A[%d][1],A[%d][2],",i,i,i);
	fprintf(fp, "Width,Height\n");

	
	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rotationVectors, translationVectors, flag);
	calibrationMatrixValues(cameraMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectRatio);
	fprintf(fp, "%d, %.12lf,%.12lf,%.12lf,%.12lf,%.12lf,%.12lf,", i, fovx,fovy, focalLength, principalPoint.x, principalPoint.y, aspectRatio);
	for(j=0;j<distCoeffs.cols;j++)
		fprintf(fp, "%.12lf,", distCoeffs.at<double>(0,j));
	for(;j<8;j++)
		fprintf(fp, ",");
	fprintf(fp, ",");

	for(j=0;j<3;j++)
		fprintf(fp, "%.12lf,%.12lf,%.12lf,", cameraMatrix.at<double>(j,0), cameraMatrix.at<double>(j,1), cameraMatrix.at<double>(j,2));
	fprintf(fp,"%d,%d\n", imageSize.width, imageSize.height);


	}


	

	fclose(fp);

	return 1;
}


 */

