#!/usr/bin/python

"""
This program is used to view left and right images and capture image pairs
to be output into a directory and used for calibrating a stereo camera set.

Author: John Keller
Date: October 11, 2016

"""

import rospy
import cv2
from sensor_msgs.msg import Image
import sys
import os
from cv_bridge import CvBridge

bridge = CvBridge()
output_dir = ''
left = None
right = None
count = 0

def save_images():
    global count
    cv2.imwrite(output_dir + 'left%04d.jpg' % (count), left)
    cv2.imwrite(output_dir + 'right%04d.jpg' % (count), right)
    count += 1
    print(count)

def left_callback(msg):
    global left
    left = bridge.imgmsg_to_cv2(msg, 'bgr8')
    cv2.imshow('left', left)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('c'):
        save_images()
    elif key == ord('q'):
        exit()

def right_callback(msg):
    global right
    right = bridge.imgmsg_to_cv2(msg, 'bgr8')
    cv2.imshow('right', right)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('c'):
        save_images()
    elif key == ord('q'):
        exit()

def print_usage():
    print('This program is used to view left and right images and capture image pairs'+
          'to be output into a directory and used for calibrating a stereo camera set.\n'+
          'Press \'c\' while the left or right image window is in focus to save the image pair.\n'+
          'Press \'q\' to quit.')
    print('Usage: ./stereo_image_capture_node.py [name of topic publishing left camera images] '+
          '[name of topic publishing right camera images] [output directory name]')

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print_usage()
        exit()
    
    # get input parameters
    left_image_topic_name = sys.argv[1]
    right_image_topic_name = sys.argv[2]
    output_dir = sys.argv[3]+'/'
    
    # create output directory to store the images
    try:
        os.mkdir(output_dir)
    except:
        pass
    
    # initialize ROS and subscribers
    rospy.init_node('stereo_image_capture_node')
    left_usb = rospy.Subscriber(left_image_topic_name, Image, left_callback)
    right_usb = rospy.Subscriber(right_image_topic_name, Image, right_callback)
    rospy.spin()
