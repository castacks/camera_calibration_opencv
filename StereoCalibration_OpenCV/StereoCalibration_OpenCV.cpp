/*
 * StereoCalibration_OpenCV.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: rsugiura
 */


/*
#ifdef _DEBUG
    //Debug���[�h�̏ꍇ
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_core245d.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_imgproc245d.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_highgui245d.lib")
	#pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_calib3d245d.lib")
#else
    //Release���[�h�̏ꍇ
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_core245.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_imgproc245.lib")
    #pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_highgui245.lib")
	#pragma comment(lib,"C:\\opencv\\build\\x86\\vc10\\lib\\opencv_calib3d245.lib")
#endif
*/



#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>

using namespace std;
using namespace cv;

#define PATTERNSIZE_X 8
#define PATTERNSIZE_Y 6

double computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
                          const vector<vector<Point2f> >& imagePoints,
                          const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                          const Mat& cameraMatrix , const Mat& distCoeffs,
                          vector<float>& perViewErrors)
{
vector<Point2f> imagePoints2;
int i, totalPoints = 0;
double totalErr = 0, err;
perViewErrors.resize(objectPoints.size());

for( i = 0; i < (int)objectPoints.size(); ++i )
{
  projectPoints( Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,  // project
                                       distCoeffs, imagePoints2);
  err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);              // difference

  int n = (int)objectPoints[i].size();
  perViewErrors[i] = (float) std::sqrt(err*err/n);                        // save for this view
  totalErr        += err*err;                                             // sum it up
  totalPoints     += n;
}

return std::sqrt(totalErr/totalPoints);              // calculate the arithmetical mean
}



bool read_images(char *dir_path, vector<vector <Point2f> > &imagePointsL, vector<vector <Point2f> > &imagePointsR, vector<vector <Point3f> > &objectPoints, vector<string> &image_name, Size &image_size)
{
	int					i, j, n=0, n_pair;
	DIR					*d;
	struct dirent		*dir;
	Mat					imgL, imgR, imgL_gray, imgR_gray;
	char				str[256], path1[256], path2[256];
	const Size			patternSize(PATTERNSIZE_X, PATTERNSIZE_Y);
	TermCriteria		criteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 40, 0.001);
	vector<Point2f>		image_pointsL;
	vector<Point2f>		image_pointsR;
	vector<Point3f>		object_points;


	d = opendir(dir_path);
	if(d){
		while((dir = readdir(d)) != NULL){
			sprintf(str, dir->d_name);
			if(str[strlen(str)-3]!='j' || str[strlen(str)-2]!='p' || str[strlen(str)-1]!='g')
				continue;
			n++;
		}
	}
	closedir(d);
	if(n<2) return false;
	n_pair=(int)(n/2.0);
	n=0;
	for(i=0;i<n_pair+100;i++){
		sprintf(path1, "%sleft%04d.jpg", dir_path, i);
		imgL = imread(path1,1);
		sprintf(path2, "%sright%04d.jpg", dir_path, i);
		imgR = imread(path2,1);
		if(imgL.data==NULL || imgR.data==NULL)
			continue;

		cvtColor(imgL, imgL_gray, CV_BGR2GRAY);
		cvtColor(imgR, imgR_gray, CV_BGR2GRAY);
		printf("L:left%04d  R:right%04d   ", i, i);
		if(!findChessboardCorners(imgL_gray, patternSize, image_pointsL)){
			printf("---enough corners not found. Skip\n");
			continue;
		}

		if(!findChessboardCorners(imgR_gray, patternSize, image_pointsR)){
			printf("---enough corners not found. Skip\n");
			continue;
		}
		printf("...All corners found from the image.\n");

		cornerSubPix(imgL_gray, image_pointsL, Size(11,11), Size(-1,-1), criteria);
		cornerSubPix(imgR_gray, image_pointsR, Size(11,11), Size(-1,-1), criteria);

		for(j=0;j<patternSize.area();j++)
			object_points.push_back(Point3f(static_cast<float>(j%patternSize.width*0.152), static_cast<float>(j/patternSize.width*0.152), 0.0));

		imagePointsL.push_back(image_pointsL);
		imagePointsR.push_back(image_pointsR);
		objectPoints.push_back(object_points);
		image_pointsL.clear();
		image_pointsR.clear();
		object_points.clear();

		image_name.push_back(path1);
		image_name.push_back(path2);

		for(j=0;j<(int)imagePointsL[n].size();j+=1)
			circle(imgL, imagePointsL[n][j], 3, Scalar(0,0,255), -1, 4);
		for(j=0;j<(int)imagePointsR[n].size();j+=1)
			circle(imgR, imagePointsR[n][j], 3, Scalar(0,0,255), -1, 4);
		imshow("imgL", imgL);
		imshow("imgR", imgR);

		image_size = imgL.size();
		waitKey(50);
		n++;
	}

	destroyWindow("imgL");
	destroyWindow("imgR");

	return true;


}




int main(int argc, char **argv)
{

	char						dir_path[512]="../calib_images_stereo_Cam02_Cam03/";
	char						str[512]="";

	vector<string>				image_name;

	vector<vector<Point2f> >	imagePoints1;
	vector<vector<Point2f> >	imagePoints2;
	vector<vector<Point3f> >	objectPoints;

	int							i,j;

	//Camera parameters
	Mat							cameraMatrix1 = (Mat_<double>(3,3)<<
			                    219.2852626710409, 0,                 324.9291651614901,
								0,                 221.0147071774210, 228.5739312205665,
								0,                 0,                 1);

							  //219.2852656816134, 0,                 324.9291656451326,
							  //0,                 221.0147114485482, 228.5739315141009,
							  //0,                 0,                 1);

	Mat							cameraMatrix2 = (Mat_<double>(3,3)<<
							    214.9685882057247, 0,                 321.3550858691472,
								0,                 216.4142254406904, 241.1723445381170,
								0,                 0,                 1);

							  //214.9685879895557, 0,                 321.3550853706877,
							  //0,                 216.4142262055010, 241.1723464083777,
							  //0,                 0,                 1);

	Mat							newCameraMatrix;

	Mat							distCoeffs1 = (Mat_<double>(1,5)<<
			-0.00830019572428760, -0.03052330227653213, 0.00099020295838697, -0.00070881601141006, 0.00427311065251838);
	      //-0.00830019610135624, -0.03052330341825751, 0.00099020168934467, -0.00070881598855560, 0.00427311096351578);

	Mat							distCoeffs2 = (Mat_<double>(1,5)<<
			0.01821751734145386, -0.04394982322391282, 0.00085442484904440, 0.00055636021150855, 0.00635748101703826);
		  //0.01821752173172169, -0.04394982585604708, 0.00085442345531448, 0.00055635993102687, 0.00635748152490955);

	Mat							R,T,E,F, R1,R2,P1,P2,Q,P1_w,P2_w;
	Rect						validPixROI1, validPixROI2;
	Mat							rmap1[2], rmap2[2];

	vector<Mat>					rotationVectors;
	vector<Mat>					translationVectors;
	int							flag; //= CV_CALIB_RATIONAL_MODEL; //CV_CALIB_FIX_K3;
	Size						imageSize, newImageSize;


	double						apertureWidth1 = 0.0, apertureHeight1 = 0.0 ;			//double apertureWidth = 23.5, apertureHeight = 15.6 ;  // �Z���T�̕����I�ȕ��E����
	double						fovx1 = 0, fovy1 = 0;                                 //
	double						focalLength1 = 0;                                    //
	Point2d						principalPoint1(0.0,0.0);                            //
	double						aspectRatio1 = 0;

	double						apertureWidth2 = 0.0, apertureHeight2 = 0.0 ;			//double apertureWidth = 23.5, apertureHeight = 15.6 ;  // �Z���T�̕����I�ȕ��E����
	double						fovx2 = 0, fovy2 = 0;                                 //
	double						focalLength2 = 0;                                    //
	Point2d						principalPoint2(0.0,0.0);                            //
	double						aspectRatio2 = 0;//

	FILE						*fp;

	Mat							img, undist_img, map1, map2;
	double						rms;

	sprintf(dir_path, argv[1]);
	int str_len = strlen(dir_path);
	if(dir_path[str_len-1]!='/'){
		dir_path[str_len]='/';
		dir_path[str_len+1]=0;
	}

	read_images(dir_path, imagePoints1, imagePoints2, objectPoints, image_name, imageSize);

	flag = 0;
	flag = CV_CALIB_FIX_INTRINSIC;
	//flag = CV_CALIB_FIX_INTRINSIC | CV_CALIB_USE_INTRINSIC_GUESS;


	rms = stereoCalibrate(objectPoints, imagePoints1, imagePoints2,
			cameraMatrix1, distCoeffs1,
			cameraMatrix2, distCoeffs2,
			imageSize, R,T,E,F,
			TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 50, 1e-6), flag);

	printf("rms = %lf\n", rms);
	//All pixel will be included in the rectified image; alpha=1.0;
	stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize,
			R, T, R1,R2,P1_w,P2_w,Q, CV_CALIB_ZERO_DISPARITY, 1.0, Size(0,0), &validPixROI1, &validPixROI2);

	//only valid pixels are visible; alpha=0.0;
	stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize,
			R, T, R1,R2,P1,P2,Q, CV_CALIB_ZERO_DISPARITY, 0.0, Size(0,0), &validPixROI1, &validPixROI2);


	initUndistortRectifyMap(cameraMatrix1, distCoeffs1, R1, P1, imageSize, CV_16SC2, rmap1[0], rmap1[1]);
	initUndistortRectifyMap(cameraMatrix2, distCoeffs2, R2, P2, imageSize, CV_16SC2, rmap2[0], rmap2[1]);

	Mat canvas;
	double sf;
	int w, h;
	sf = 600./MAX(imageSize.width, imageSize.height);
	w = cvRound(imageSize.width*sf);
	h = cvRound(imageSize.height*sf);
	canvas.create(h, w*2, CV_8UC3);

	for(i=0; i<image_name.size();i+=2){
       Mat img = imread(image_name[i], 0), rimg, cimg;
       remap(img, rimg, rmap1[0], rmap1[1], INTER_LINEAR);
       cvtColor(rimg, cimg, COLOR_GRAY2BGR);
       Mat canvasPart = canvas(Rect(w*0, 0, w, h));
       resize(cimg, canvasPart, canvasPart.size(), 0, 0, INTER_AREA);

       Rect vroi1(cvRound(validPixROI1.x*sf), cvRound(validPixROI1.y*sf),
                 cvRound(validPixROI1.width*sf), cvRound(validPixROI1.height*sf));
       rectangle(canvasPart, vroi1, Scalar(0,0,255), 3, 8);

       img = imread(image_name[i+1], 0);
       remap(img, rimg, rmap2[0], rmap2[1], INTER_LINEAR);
       cvtColor(rimg, cimg, COLOR_GRAY2BGR);
       canvasPart = canvas(Rect(w*1, 0, w, h));
       resize(cimg, canvasPart, canvasPart.size(), 0, 0, INTER_AREA);

       Rect vroi2(cvRound(validPixROI2.x*sf), cvRound(validPixROI2.y*sf),
                 cvRound(validPixROI2.width*sf), cvRound(validPixROI2.height*sf));
       rectangle(canvasPart, vroi2, Scalar(0,0,255), 3, 8);

       for(j=0; j<canvas.rows; j+=16)
    	   line(canvas, Point(0, j), Point(canvas.cols, j), Scalar(0, 255, 0), 1, 8);
       imshow("rectified", canvas);
       waitKey(0);
	}


	calibrationMatrixValues(cameraMatrix1, imageSize, apertureWidth1, apertureHeight1, fovx1, fovy1, focalLength1, principalPoint1, aspectRatio1);
	calibrationMatrixValues(cameraMatrix2, imageSize, apertureWidth2, apertureHeight2, fovx2, fovy2, focalLength2, principalPoint2, aspectRatio2);

	memset(str, 0, 512);
	sprintf(str, "%sstereo_left.yaml", dir_path);
	fp=fopen(str, "w");
	fprintf(fp, "image_width: %d\n", imageSize.width);
	fprintf(fp, "image_height: %d\n", imageSize.height);

	fprintf(fp, "camera_name: head_camera\n");
	fprintf(fp, "camera_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, 0, %.13lf, %.13lf, 0, 0, 1]\n", cameraMatrix1.at<double>(0,0), cameraMatrix1.at<double>(0,2), cameraMatrix1.at<double>(1,1), cameraMatrix1.at<double>(1,2));
	fprintf(fp, "distortion_model: plumb_bob\n");
	fprintf(fp, "distortion_coefficients:\n");
	fprintf(fp, "  rows: 1\n");
	fprintf(fp, "  cols: 5\n");
	fprintf(fp, "  data: [%.17lf, %.17lf, %.17lf, %.17lf, %.17lf]\n", distCoeffs1.at<double>(0,0), distCoeffs1.at<double>(0,1), distCoeffs1.at<double>(0,2), distCoeffs1.at<double>(0,3), distCoeffs1.at<double>(0,4));
	fprintf(fp, "rectification_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [%.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf]\n",
						R1.at<double>(0,0), R1.at<double>(0,1), R1.at<double>(0,2), R1.at<double>(1,0), R1.at<double>(1,1), R1.at<double>(1,2), R1.at<double>(2,0), R1.at<double>(2,1), R1.at<double>(2,2));
	fprintf(fp, "projection_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 4\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, 0, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n",  P1.at<double>(0,0), P1.at<double>(0,2), P1.at<double>(1,1), P1.at<double>(1,2));
	fprintf(fp, " #data: [%.13lf, 0, %.13lf, 0, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n",  P1_w.at<double>(0,0), P1_w.at<double>(0,2), P1_w.at<double>(1,1), P1_w.at<double>(1,2));

	fprintf(fp, "#\n");
	fprintf(fp, "#Output field of view in degrees along the horizontal sensor axis: %lf\n", fovx1);
	fprintf(fp, "#Output field of view in degrees along the vertical sensor axis: %lf\n", fovy1);
	fprintf(fp, "#Focal length of the lens in mm: %lf\n", focalLength1);
	fprintf(fp, "#Principal point (x,y) in mm: %lf, %lf\n", principalPoint1.x, principalPoint1.y);
	fprintf(fp, "#Aspect ratio fx/fy: %lf\n", aspectRatio1);
	fprintf(fp, "#RMS error of reprojection: %lf\n", rms);

	fclose(fp);

	memset(str, 0, 512);
	sprintf(str, "%sstereo_right.yaml", dir_path);
	fp=fopen(str, "w");
	fprintf(fp, "image_width: %d\n", imageSize.width);
	fprintf(fp, "image_height: %d\n", imageSize.height);

	fprintf(fp, "camera_name: head_camera\n");
	fprintf(fp, "camera_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, 0, %.13lf, %.13lf, 0, 0, 1]\n", cameraMatrix2.at<double>(0,0), cameraMatrix2.at<double>(0,2), cameraMatrix2.at<double>(1,1), cameraMatrix2.at<double>(1,2));
	fprintf(fp, "distortion_model: plumb_bob\n");
	fprintf(fp, "distortion_coefficients:\n");
	fprintf(fp, "  rows: 1\n");
	fprintf(fp, "  cols: 5\n");
	fprintf(fp, "  data: [%.17lf, %.17lf, %.17lf, %.17lf, %.17lf]\n", distCoeffs2.at<double>(0,0), distCoeffs2.at<double>(0,1), distCoeffs2.at<double>(0,2), distCoeffs2.at<double>(0,3), distCoeffs2.at<double>(0,4));
	fprintf(fp, "rectification_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 3\n");
	fprintf(fp, "  data: [%.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf, %.13lf]\n",
					R2.at<double>(0,0), R2.at<double>(0,1), R2.at<double>(0,2), R2.at<double>(1,0), R2.at<double>(1,1), R2.at<double>(1,2), R2.at<double>(2,0), R2.at<double>(2,1), R2.at<double>(2,2));
	fprintf(fp, "projection_matrix:\n");
	fprintf(fp, "  rows: 3\n");
	fprintf(fp, "  cols: 4\n");
	fprintf(fp, "  data: [%.13lf, 0, %.13lf, %.13lf, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n", P2.at<double>(0,0), P2.at<double>(0,2), P2.at<double>(0,3), P2.at<double>(1,1), P2.at<double>(1,2));
	fprintf(fp, " #data: [%.13lf, 0, %.13lf, %.13lf, 0, %.13lf, %.13lf, 0, 0, 0, 1, 0]\n", P2_w.at<double>(0,0), P2_w.at<double>(0,2), P2_w.at<double>(0,3), P2_w.at<double>(1,1), P2_w.at<double>(1,2));

	fprintf(fp, "#\n");
	fprintf(fp, "#Output field of view in degrees along the horizontal sensor axis: %lf\n", fovx2);
	fprintf(fp, "#Output field of view in degrees along the vertical sensor axis: %lf\n", fovy2);
	fprintf(fp, "#Focal length of the lens in mm: %lf\n", focalLength2);
	fprintf(fp, "#Principal point (x,y) in mm: %lf, %lf\n", principalPoint2.x, principalPoint2.y);
	fprintf(fp, "#Aspect ratio fx/fy: %lf\n", aspectRatio2);
	fprintf(fp, "#RMS error of reprojection: %lf\n", rms);
	fprintf(fp, "#Length of the base line in m: %lf\n", -1.0*P2.at<double>(0,3)/P2.at<double>(0,0));

	fclose(fp);


	return 1;
}

